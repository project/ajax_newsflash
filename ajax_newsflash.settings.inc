<?php
/**
 * @file
 * Settings page callback file for the ajax_newsflash module.
 */

/**
 * Menu callback;
 */
 
function ajax_newsflash_admin_settings() {

  // only administrators can access this function
  
  // Generate the form - settings applying to all patterns first
  $form['ajax_newsflash_settings'] = array(
    '#type' => 'fieldset',
    '#weight' => -20,
    '#title' => t('Basic settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE
  );
    
  $form['ajax_newsflash_settings']['ajax_newsflash_more'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show "more" link.'),
	  '#default_value' => variable_get('ajax_newsflash_more', 1),
    '#description' => t("Display 'more' link below the Newsflash content on the block."),           
    '#maxlength' => '1', '#size' => '1');
  
	$form['ajax_newsflash_settings']['ajax_newsflash_item'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of Newsflash items per page'),
	  '#default_value' => variable_get('ajax_newsflash_item', 10),
    '#description' => t("How many content will be rotated.")
    );
	$form['ajax_newsflash_settings']['ajax_newsflash_source'] = array(
    '#type' => 'textfield',
    '#title' => t('Content type used for Ajax Newsflash'),
	  '#default_value' => variable_get('ajax_newsflash_source', 'page'),
    '#description' => t("Content type used for Ajax Newsflash source.")
    );
	$form['ajax_newsflash_settings']['ajax_newsflash_content'] = array(
    '#type' => 'select',
    '#title' => t('Content used for Ajax Newsflash'),
		'#options' => array('title'=>'title','teaser'=>'teaser'),
	  '#default_value' => variable_get('ajax_newsflash_content', 'teaser'),
    '#description' => t("Content used for Ajax Newsflash source.")
    );	
	$form['ajax_newsflash_settings']['ajax_newsflash_delay'] = array(
    '#type' => 'textfield',
    '#title' => t('Ajax Newsflash delay'),
	  '#default_value' => variable_get('ajax_newsflash_delay', '3500')
    );	  
  return system_settings_form($form);
}
 

